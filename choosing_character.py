import pygame
from time import monotonic

pygame.init()
choose_bg = pygame.image.load('./bg/old.png')


def choose_character(size):
    choosing_win = pygame.display.set_mode(size)
    pygame.display.set_caption('My Game')

    class RPG_character(object):
        def __init__(self, type, is_active, coord_x, coord_y):
            self.type = type
            self.is_active = is_active
            self.x = coord_x
            self.y = coord_y

        def draw(self):

            time = monotonic()
            player = 2 * [pygame.image.load(f'./choose/{self.type}1.png'),
                          pygame.image.load(f'./choose/{self.type}2.png'),
                          pygame.image.load(f'./choose/{self.type}3.png')]

            if monotonic() - time >= 30:
                time = monotonic()

            choosing_win.blit(choose_bg, (0, 0))
            choosing_win.blit(player[int(2*time) % 6], (size[0] // 2 + 50, size[1] // 2 + 20))
            pygame.draw.circle(choosing_win, (255, 255, 255), (self.x, self.y), 5)
            pygame.display.update()


    run_choosing_win = True
    char_count = 0
    count = 0
    choosing_win.blit(choose_bg, (0, 0))
    pygame.display.update()
    while run_choosing_win:
        pygame.time.Clock().tick(30)
        # стандартный выход из игры
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run_choosing_win = False

        # основной процесс выбора
        characters = [RPG_character('warrior', False, size[0]//5, size[1]//2 + 40),
                      RPG_character('mage', False, size[0]//5, size[1]//2 + 65)]

        keys_pressed = pygame.key.get_pressed()
        # сделать адекватный цикл
        if char_count == 0:
            character = characters[char_count % 2]
            character.is_active = True
            character.draw()
            if keys_pressed[pygame.K_e]:
                choosed_character = character
                run_choosing_win = False
            elif keys_pressed[pygame.K_DOWN] or keys_pressed[pygame.K_s]:
                char_count = 1

        elif char_count == 1:
            character = characters[char_count % 2]
            character.is_active = True
            character.draw()
            if keys_pressed[pygame.K_e]:
                choosed_character = character
                run_choosing_win = False
            elif keys_pressed[pygame.K_UP] or keys_pressed[pygame.K_w]:
                char_count = 0

    return choosed_character.type
