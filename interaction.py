from enemy import Skeleton


def interaction_manager(player, lian, fireballs, slimes, ghosts, dark_mage, graves, skeletons, add_hearts, chest):
    """ player-lian """
    if abs(lian.right[0][1] - player.x + player.distance + 70) < 40 and player.is_sword:
        lian.health -= 0.1

    """ player - add_hearts """
    for add_heart in add_hearts:
        if abs(add_heart.x - player.x + player.distance) + abs(player.y - add_heart.y) < 30 and add_heart.is_exist:
            player.health = min(12, player.health + add_heart.healing)
            add_heart.is_exist = False

    """ player-chest """
    if abs(chest.x - player.x + player.distance) < 70:
        chest.is_open = True

    """ player-slime """
    for slime in slimes:
        player_slime_dist = abs((player.x + player.width/2)-(slime.x + player.distance + slime.width/2))
        if player_slime_dist < 90 and slime.is_alive:
            player.health -= slime.poison

            if (player_slime_dist < slime.width / 2) and player.is_jump and abs(player.y - slime.height[0]) < 10:
                slime.jump_fight()

            if (player_slime_dist < slime.width) and player.is_sword and abs(player.y - slime.height[0]) < 10:
                slime.health -= 1

        for fire in fireballs:
            fire_slime_dist = ((fire.x - (slime.x + player.distance + slime.width/2))**2
                               + (fire.y - slime.height[0])**2)**0.5
            if fire_slime_dist < slime.width/2 and slime.is_alive:
                slime.health -= 1
                fireballs.pop(fireballs.index(fire))

    """ player-ghost """
    for ghost in ghosts:
        player_ghost_dist = (((player.x + player.width / 2) - (ghost.x + player.distance + ghost.width / 2))**2 +
                             ((player.y + player.height / 2) - (ghost.y + ghost.height / 2)) ** 2)**0.5

        if player_ghost_dist < 45 and ghost.is_alive:
            player.health -= 0.5
            ghost.cry = True
        else:
            ghost.cry = False

        for fire in fireballs:
            fire_ghost_dist = ((fire.x - (ghost.x + player.distance + ghost.width / 2)) ** 2
                               + (fire.y - ghost.y - ghost.height / 2) ** 2) ** 0.5
            if fire_ghost_dist < 10 and ghost.is_alive:
                ghost.health -= 1
                fireballs.pop(fireballs.index(fire))

    """ player - dark mage """
    player_darkmage_dist = ((player.x + player.width/2 - dark_mage.x - player.distance - dark_mage.width/2)**2 +
                            (player.y + player.height/2 - dark_mage.y - dark_mage.height/2)**2)**0.5
    graves_coord = []
    if player_darkmage_dist < 100:
        graves_coord = [grave.coord for grave in graves]
        dark_mage.attack()
    else:
        dark_mage.x = 5000

    if dark_mage.is_magic and player_darkmage_dist < 150 and player.y > 200:
        player.health -= 0.5

    dead_skeletons = 4
    for skeleton in skeletons:
        if skeleton.is_alive:
            dead_skeletons -= 1
    if dark_mage.is_attack and dead_skeletons == 4:
        dark_mage.is_attack = False
        dark_mage.y = 220

    if player_darkmage_dist < 20 and player.is_sword:
        dark_mage.health -= 0.09

    for fire in fireballs:
        fire_dm_dist = ((fire.x - (dark_mage.x + player.distance + dark_mage.width / 2)) ** 2
                        + (fire.y - dark_mage.y - dark_mage.height / 2) ** 2) ** 0.5
        if fire_dm_dist < 20 and dark_mage.is_alive:
            dark_mage.health -= 0.09
            fireballs.pop(fireballs.index(fire))

    """ player - dark mage's fireballs """
    for fire in dark_mage.fires:
        if -100 < fire.y < 270:
            fire.move()
            fire_player_dist = ((player.x + player.width/2 - player.distance - fire.x)**2 +
                                (fire.y - player.y - player.height / 2)**2)**0.5
            if fire_player_dist < player.width/2:
                player.health -= 0.3
                dark_mage.fires.pop(dark_mage.fires.index(fire))
        else:
            dark_mage.fires.pop(dark_mage.fires.index(fire))

    """ player-skeleton """
    for skeleton in skeletons:
        player_sk_dist = (((player.x + player.width / 2) - (skeleton.x + player.distance + skeleton.width / 2)) ** 2 +
                          ((player.y + player.height / 2) - (skeleton.y + skeleton.height / 2)) ** 2) ** 0.5
        if player_sk_dist < 15 and skeleton.is_alive:
            player.health -= 0.1

        if player_sk_dist < skeleton.width and player.is_sword:
            skeleton.health -= 0.5

        for fire in fireballs:
            fire_sk_dist = ((fire.x - (skeleton.x + player.distance + skeleton.width / 2)) ** 2
                            + (fire.y - skeleton.y - skeleton.height / 2) ** 2) ** 0.5
            if fire_sk_dist < 10 and skeleton.is_alive:
                skeleton.health -= 0.4
                fireballs.pop(fireballs.index(fire))

    return [Skeleton(coord) for coord in graves_coord]
