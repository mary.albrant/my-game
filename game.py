import pygame
import choosing_character as cc
import objects
import enemy
import interaction
from location import set_blocks_in_location, Lian, ExtraHearts, Chest

pygame.init()
size = (int(510), int(300))
win = pygame.display.set_mode(size)
pygame.display.set_caption('My Game')


character_ = cc.choose_character(size)
player = objects.Player(character_, False, False, 'right', 0, -11)
flame = pygame.image.load(f'./character/{character_}/flame.png')
slimes, ghosts, dark_mage, graves = enemy.set_enemies()
skeletons = []
platforms = set_blocks_in_location()
liana = Lian(4280, 4200, 400, 150)
add_hearts = [ExtraHearts(1690, 150), ExtraHearts(2450, 150), ExtraHearts(4250, 250)]
chest = Chest(6000)
window_bg = objects.WindowBg()


fireballs = []
run = True
while run:
    pygame.time.Clock().tick(30)
    # стандартный выход
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False

    # движение снарядов и удаление лишних
    for fireball in fireballs:
        if 0 < fireball.x < size[0]:
            fireball.x += fireball.velocity
        else:
            fireballs.pop(fireballs.index(fireball))

    if skeletons == []:
        skeletons = interaction.interaction_manager(player, liana, fireballs, slimes, ghosts, dark_mage, graves,
                                                    skeletons, add_hearts, chest)
    else:
        interaction.interaction_manager(player, liana, fireballs, slimes, ghosts, dark_mage, graves, skeletons,
                                        add_hearts, chest)

    pressed_keys = pygame.key.get_pressed()
    player.movement(pressed_keys, platforms)

    if pressed_keys[pygame.K_f]:
        if player.last_move == 'right':
            face = 1
        else:
            face = -1
        if len(fireballs) < 5:
            fireballs.append(objects.Fireball(flame, round(player.x + player.width//2),
                                              round(player.y + player.height//2 - 3), face))

    objects.draw_window_item(win, window_bg, player, fireballs, slimes, ghosts, dark_mage, graves, skeletons, liana,
                             add_hearts, chest)

pygame.quit()
