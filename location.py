import pygame


class Block(object):
    def __init__(self, x1, x2, y1, y2):
        """
        [x1, y2] ------ up ------ [x2, y2]
          |                         |
        left                       right
          |                         |
        [x1, y1] ----- down ----- [x2, y1]
        """
        self.left = [[x1, x1], [y1, y2]]
        self.right = [[x2, x2], [y1, y2]]
        self.up = [[x1, x2], [y2, y2]]
        self.down = [[x1, x2], [y1, y2]]
        self.par = [x2, y2, x1-x2, y1-y2]


def set_blocks_in_location():
    """ idk why, but x_coord must be inverted """
    tree = [Block(2880, 2200, 400, 180)]
    stones = [Block(1630, 1555, 400, 200), Block(1720, 1630, 400, 150)]
    arch = [Block(4145, 4095, 400, 190), Block(4280, 4145, 200, 150)]
    blocks = [*tree, *stones, *arch]
    return blocks


class Lian(Block):
    def __init__(self, x1, x2, y1, y2):
        Block.__init__(self, x1, x2, y1, y2)
        self.health = 5

        self.image = pygame.image.load('./bg/liana.png')

    def draw(self, window, player_distance):
        if self.health > 0:
            window.blit(self.image, (self.right[0][0] + player_distance + 50, 0))


class ExtraHearts(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.healing = 2
        self.is_exist = True
        self.image = pygame.image.load('./hearts/1.png')

    def draw(self, window, player_distance):
        if self.is_exist:
            window.blit(self.image, (self.x + player_distance, self.y))


class Chest(object):
    def __init__(self, x):
        self.x = x
        self.is_open = False
        self.image_open = pygame.image.load('./bg/chest_opened.png')
        self.image_close = pygame.image.load('./bg/chest_closed.png')
        self.image_hb = pygame.image.load('./bg/hb.png')

    def draw(self, window, player_distance):
        if self.is_open:
            window.blit(self.image_open, (self.x + player_distance, 200))
            window.blit(self.image_hb, (self.x + player_distance - 255, 0))
        else:
            window.blit(self.image_close, (self.x + player_distance, 200))
