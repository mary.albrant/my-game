import pygame
from time import monotonic


class Player(object):
    def __init__(self, character, right, left, last_move, anim_count, jump_count):

        self.walk_right = 2 * [pygame.image.load(f'./character/{character}/rw.png'),
                               pygame.image.load(f'./character/{character}/rw1.png'),
                               pygame.image.load(f'./character/{character}/rw2.png')]
        self.walk_left = 2 * [pygame.image.load(f'./character/{character}/lw.png'),
                              pygame.image.load(f'./character/{character}/lw1.png'),
                              pygame.image.load(f'./character/{character}/lw2.png')]
        self.stand = pygame.image.load(f'./character/{character}/s1.png')

        self.x = 50
        self.y = 240
        self.y_down = 240
        self.y_up = -10
        self.width = 50
        self.height = 50
        self.speed = 5

        self.distance = 0

        self.right = right
        self.left = left
        self.is_jump = False
        self.last_move = last_move

        self.anim_count = anim_count
        self.jump_count = jump_count
        self.active_platform = 'none'

        self.health = 12
        self.is_game_over = False
        self.is_sword = False

    def movement(self, keys_pressed, platforms):

        if keys_pressed[pygame.K_LEFT] or keys_pressed[pygame.K_a]:
            block = -100
            for platform in platforms:
                if platform.left[1][0] > self.y + self.height > platform.left[1][1] \
                        and abs(self.x - platform.left[0][0] - self.distance) < 5:
                    block = platform.left[0][0] + self.distance
            self.left = True
            self.right = False
            self.last_move = 'left'
            if not (0 < self.distance < 50 and self.x == 50) and self.x != block:
                self.distance += self.speed
                if self.x > 50:
                    self.x -= self.speed

        elif keys_pressed[pygame.K_RIGHT] or keys_pressed[pygame.K_d]:
            block = 15000
            for platform in platforms:
                if platform.right[1][0] > self.y + self.height > platform.right[1][1] \
                        and abs(self.x - platform.right[0][0] - self.distance) < 5:
                    block = platform.right[0][0] + self.distance
            self.left = False
            self.right = True
            self.last_move = 'right'
            if self.x != block:
                self.distance -= self.speed
                if self.x < 370 - self.width:
                    self.x += self.speed
        else:
            self.left = False
            self.right = False
            self.anim_count = 0

        self.active_platform = 'none'
        for platform in platforms:
            if platform.down[0][1] < self.x - self.distance < platform.down[0][0]:
                self.active_platform = platform
                self.y_up = -10  # active_platform.up[1][0]
                self.y_down = self.active_platform.down[1][1]
                if keys_pressed[pygame.K_SPACE] and self.y == self.y_down:
                    self.jump_count = 10
                    self.is_jump = True
                self.jump()
                break

        if self.active_platform == 'none':
            self.y_up = -10
            self.y_down = 240
            if keys_pressed[pygame.K_SPACE] and abs(self.y - self.y_down) < 5:
                self.jump_count = 10
                self.is_jump = True
            self.jump()
            if not self.is_jump:
                self.jump_count = -11
                self.fall(platforms)

        if keys_pressed[pygame.K_e]:
            self.is_sword = not self.is_sword

    def jump(self):
        if self.jump_count >= -10 and self.is_jump:
            if self.jump_count >= 0:
                if self.y_up < self.y:
                    self.y -= (self.jump_count ** 3) / (3 * abs(self.jump_count) + 1)
                    self.jump_count -= 1
                else:
                    self.jump_count = -self.jump_count

            if self.jump_count <= 0:
                if self.y_down > self.y:
                    self.y -= (self.jump_count ** 3) / (3 * abs(self.jump_count) + 1)
                    self.jump_count -= 1
                else:
                    self.y = min(self.y_down, 240)
                    self.is_jump = False
        else:
            self.is_jump = False

    def fall(self, platforms):
        floor = 290
        for platform in platforms:
            if platform.down[0][1] < self.x - self.distance < platform.down[0][0]:
                floor = platform.down[1][1]
        if self.y - floor + self.height < 0:
            self.y += 7

    def draw(self, window):
        if self.health > 0:
            hearts = pygame.image.load(f'./hearts/{int(self.health) // 2}.png')
            window.blit(hearts, (10, 10))
        else:
            self.is_game_over = True

        sword_left = 3 * [pygame.image.load(f'./character/sword/left1.png'),
                          pygame.image.load(f'./character/sword/left2.png')]

        sword_right = 3 * [pygame.image.load(f'./character/sword/right2.png'),
                           pygame.image.load(f'./character/sword/right1.png')]

        time = monotonic()

        if monotonic() - time <= 30:
            time = monotonic()

        if self.anim_count >= 30:
            self.anim_count = 0

        if self.left:
            window.blit(self.walk_left[self.anim_count // 5], (self.x, self.y))
            self.anim_count += 1
        elif self.right:
            window.blit(self.walk_right[self.anim_count // 5], (self.x, self.y))
            self.anim_count += 1
        else:
            window.blit(self.stand, (self.x, self.y))
            self.anim_count += 1

        if self.is_sword:
            if self.last_move == 'left':
                window.blit(sword_right[int(7 * time) % 6], (self.x - 20, self.y))
            else:
                window.blit(sword_left[int(7 * time) % 6], (self.x, self.y))


class Fireball(object):
    def __init__(self, flame, x, y, facing):
        self.flame = flame
        self.x = x
        self.y = y
        self.facing = facing
        self.velocity = 8 * facing

    def draw(self, window):
        window.blit(self.flame, (self.x, self.y))


class WindowBg(object):
    def __init__(self):
        self.bg = pygame.image.load('./bg/mainbg.png')

    class Mountains(object):
        def __init__(self, x):
            self.image = pygame.image.load('./bg/mountainsbg.png')
            self.x = -10 + x/20

        def draw(self, window):
            window.blit(self.image, (self.x, 0))

    class Trees(object):
        def __init__(self, x):
            self.image = pygame.image.load('./bg/treesbg.png')
            self.x = -10 + x/5

        def draw(self, window):
            window.blit(self.image, (self.x, 0))

    class Grass(object):
        def __init__(self, x):
            self.image = pygame.image.load('./bg/grassbg.png')
            self.x = -10 + x

        def draw(self, window):
            window.blit(self.image, (self.x, 0))

    def draw(self, window, distance):
        window.blit(self.bg, (0, 0))

        self.Mountains(distance).draw(window)
        self.Trees(distance).draw(window)
        self.Grass(distance).draw(window)


def draw_window_item(window, window_bg, player, fireballs, slimes, ghosts, dark_mage, graves, skeletons, lian,
                     add_hearts, chest):
    window_bg.draw(window, player.distance)
    player.draw(window)
    for fireball in fireballs:
        fireball.draw(window)
    for slime in slimes:
        slime.draw(window, player.distance)
    for ghost in ghosts:
        ghost.draw(window, player)
    for grave in graves:
        grave.draw(window, player)
    dark_mage.draw(window, player)
    for skeleton in skeletons:
        skeleton.draw(window, player)
    for fire in dark_mage.fires:
        fire.draw(window, player)
    lian.draw(window, player.distance)
    chest.draw(window, player.distance)
    for add_heart in add_hearts:
        add_heart.draw(window, player.distance)
    if player.is_game_over:
        image = pygame.image.load('./bg/game_over.png')
        window.blit(image, (0, 0))
    pygame.display.update()
