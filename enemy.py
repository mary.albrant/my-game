import pygame
from time import monotonic


class Slime(object):
    def __init__(self, coord_x=400):
        self.image = 3 * [pygame.image.load(f'./slimes/green1.png'),
                          pygame.image.load(f'./slimes/green2.png')]
        self.image_poison = 3 * [pygame.image.load(f'./slimes/poison1.png'),
                                 pygame.image.load(f'./slimes/poison2.png')]

        self.x = coord_x
        self.state_x = coord_x + 10
        self.height = (245, 275)
        self.width = 45

        self.is_alive = True
        self.health = 14
        self.poison = 0.1

    def movement(self):
        time = monotonic()
        self.x += ((time % 2) - 1)/abs((time % 2) - 1)

    def draw(self, window, distance):

        if self.health <= 0:
            self.is_alive = False
        hearts = pygame.image.load(f'./health/health{int(self.health) // 2}.png')
        window.blit(hearts, (self.x + distance, 240))

        if self.is_alive:
            self.movement()
            time = monotonic()
            if monotonic() - time >= 30:
                time = monotonic()
            window.blit(self.image[int(2 * time) % 6], (self.x + distance, 245))
            window.blit(self.image_poison[int(2 * time) % 6], (self.state_x + distance - 90, 155))
        else:
            time = monotonic()
            self.image = 6*[pygame.image.load(f'./slimes/green3.png')]
            if monotonic() - time <= 30:
                window.blit(self.image[int(2 * time) % 6], (self.x + distance, 245))

    def jump_fight(self):
        self.health = 0


class AbstractEnemyWithMove(object):
    def __init__(self, name, coord_x, coord_y, height, width, health):
        self.name = name
        self.image_left = 2 * [pygame.image.load(f'./{name}/left1.png'),
                               pygame.image.load(f'./{name}/left2.png'),
                               pygame.image.load(f'./{name}/left3.png')]

        self.image_right = 2 * [pygame.image.load(f'./{name}/right1.png'),
                                pygame.image.load(f'./{name}/right2.png'),
                                pygame.image.load(f'./{name}/right3.png')]

        self.x = coord_x
        self.y = coord_y
        self.height = height
        self.width = width

        self.is_alive = True
        self.health = health

    def movement(self, player):
        dist = (player.x + player.width/2)-(self.x + player.distance + self.width/2)
        if dist != 0:
            self.x += dist/abs(dist)

        if dist > 0:
            return 'right'
        else:
            return 'left'

    def draw(self, window, player):

        if int(self.health) <= 0:
            self.is_alive = False
            self.health = 0
        hearts = pygame.image.load(f'./health/health{int(self.health)}.png')
        window.blit(hearts, (self.x + player.distance, self.y - 20))

        if self.is_alive:
            direction = self.movement(player)

            time = monotonic()
            if monotonic() - time >= 30:
                time = monotonic()

            if direction == 'right':
                window.blit(self.image_right[int(2 * time) % 6], (self.x + player.distance, self.y))
            else:
                window.blit(self.image_left[int(2 * time) % 6], (self.x + player.distance, self.y))

        else:
            time = monotonic()
            image = 6*[pygame.image.load(f'./{self.name}/dead.png')]
            if monotonic() - time <= 30:
                window.blit(image[int(2 * time) % 6], (self.x + player.distance, 250))


class Ghost(AbstractEnemyWithMove):
    def __init__(self, coord_x):
        AbstractEnemyWithMove.__init__(self,  name='ghost', coord_x=coord_x, coord_y=235, height=48, width=38, health=7)
        self.cry = False
        self.velocity = 2

    def movement(self, player):
        time = monotonic()
        self.y += ((time % 2) - 1) / abs((time % 2) - 1) * self.velocity
        return AbstractEnemyWithMove.movement(self, player)

    def draw(self, window, player):
        AbstractEnemyWithMove.draw(self, window, player)
        if self.is_alive and self.cry:
            image = pygame.image.load(f'./ghost/cry.png')
            window.blit(image, (self.x + player.distance, self.y))


class Grave(object):
    def __init__(self, coord, direction):
        self.image = pygame.image.load(f'./skeleton/grave{direction}.png')
        self.coord = coord

    def draw(self, window, player):
        window.blit(self.image, (self.coord + player.distance, 240))


class Skeleton(AbstractEnemyWithMove):
    def __init__(self, coord_x):
        AbstractEnemyWithMove.__init__(self,  name='skeleton', coord_x=coord_x, coord_y=240, height=48, width=38,
                                       health=7)
        self.velocity = 2

    def movement(self, player):
        return AbstractEnemyWithMove.movement(self, player)

    def draw(self, window, player):
        AbstractEnemyWithMove.draw(self, window, player)


class DarkWizard(AbstractEnemyWithMove):
    def __init__(self, coord_x):
        AbstractEnemyWithMove.__init__(self, 'dark_mage', coord_x, 220, 60, 64, 7)
        self.is_attack = False
        self.is_magic = False
        self.fires = []

    def movement(self, player):
        AbstractEnemyWithMove.movement(self, player)
        time = monotonic()
        self.y += ((time % 2) - 1) / abs((time % 2) - 1) * 2

        if self.is_attack and int(time) % 2 == 0 and len(self.fires) < 5:
            self.fires.append(Fire(dark_mage_coord=[self.x + self.width/2, self.y + self.height/2],
                                   player=player))

    def attack(self):
        self.y = 50
        self.is_attack = True

    def draw(self, window, player):
        AbstractEnemyWithMove.draw(self, window, player)

        if self.is_alive and not self.is_attack:
            time = monotonic()
            image_magic = pygame.image.load(f'./dark_mage/fire_attac.png')
            if int(time) % 5 == 0:
                self.is_magic = True
                window.blit(image_magic, (self.x + player.distance - 127, 200))
            else:
                self.is_magic = False


class Fire(object):
    def __init__(self, dark_mage_coord, player):
        self.x = dark_mage_coord[0]
        self.y = dark_mage_coord[1]
        self.y_vel = 5
        if abs(self.y - player.y) > 1:
            self.x_vel = - self.y_vel * (player.x - self.x - player.distance) / (self.y - player.y)
        else:
            self.x_vel = 1

        self.image_fire = pygame.image.load(f'./dark_mage/flame.png')

    def move(self):
        self.x += self.x_vel
        self.y += self.y_vel

    def draw(self, window, player):
        window.blit(self.image_fire, (self.x + player.distance, self.y))


def set_enemies():
    return [Slime(550), Slime(1350), Slime(3200), Slime(3300), Slime(3800)], \
           [Ghost(1000), Ghost(1100), Ghost(1600), Ghost(1600), Ghost(4000), Ghost(4700)], \
           DarkWizard(5000), [Grave(5000 + (-1) ** (i % 2) * (i * 50 + 50), 1 + i // 2) for i in range(4)]
